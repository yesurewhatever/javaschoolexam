package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        if (inputNumbers.contains(null)) {
            throw new CannotBuildPyramidException();
        }

        int[][] pyramid = makeSkeleton(inputNumbers.size());

        Collections.sort(inputNumbers);
        fillPyramid(pyramid, inputNumbers);

        return pyramid;
    }

    private int[][] makeSkeleton(int length) {
        for (int i = 1, j = 1; i <= length && i > 0; i += ++j) {
            if (i == length) {
                return new int[j][j + j - 1];
            }
        }
        throw new CannotBuildPyramidException();
    }

    private void fillPyramid(int[][] pyramid, List<Integer> numbers) {
        Iterator<Integer> iterator = numbers.iterator();

        int center = pyramid[0].length / 2;
        for (int i = 0; i < pyramid.length; i++) {
            if (i % 2 == 0) {
                for (int j = i / 2; j > 0 ; j--) {
                    pyramid[i][center - j * 2] = iterator.next();
                }
                pyramid[i][center] = iterator.next();
                for (int j = 1; j <= i / 2 ; j++) {
                    pyramid[i][center + j * 2] = iterator.next();
                }
            } else {
                for (int j = i / 2; j >= 0 ; j--) {
                    pyramid[i][center - j * 2 - 1] = iterator.next();
                }
                for (int j = 0; j <= i / 2 ; j++) {
                    pyramid[i][center + j * 2 + 1] = iterator.next();
                }
            }
        }
    }
}
