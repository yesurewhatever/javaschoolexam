package com.tsystems.javaschool.tasks.calculator;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayDeque;
import java.util.Deque;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        if (statement == null || !statement.matches("[0-9.()+\\-*/]+")) {
            return null;
        }

        String postfix = convertInfixToPostfix(statement);
        if (postfix == null) {
            return null;
        }

        String[] parts = postfix.split("\\s+");

        Deque<Double> stack = new ArrayDeque<>();

        for (String part : parts) {
            if (part.length() == 1 && isOperator(part.charAt(0))) {
                if (stack.size() < 2) {
                    return null;
                }

                Double d = performOperation(stack.pop(), stack.pop(), part);

                if (d == null || d.isInfinite()) {
                    return null;
                } else {
                    stack.push(d);
                }
            } else {
                try {
                    stack.push(Double.parseDouble(part));
                } catch (NumberFormatException e) {
                    return null;
                }
            }
        }

        Double res = stack.pop();

        DecimalFormat df = new DecimalFormat("#.####");
        df.setRoundingMode(RoundingMode.HALF_UP);
        return df.format(res);
    }

    private Double performOperation(double operand2, double operand1, String operation) {
        switch (operation) {
            case "+":
                return operand1 + operand2;
            case "-":
                return operand1 - operand2;
            case "*":
                return operand1 * operand2;
            case "/":
                return operand1 / operand2;
            default:
                return null;
        }
    }

    private String convertInfixToPostfix(String infix) {
        Deque<Character> stack = new ArrayDeque<>();
        StringBuilder sb = new StringBuilder(infix.length());
        boolean flagForSpaceInsertion = false;

        for (char c : infix.toCharArray()) {
            if (!isOperator(c)) {
                if (flagForSpaceInsertion) {
                    sb.append(" ");
                    flagForSpaceInsertion = false;
                }

                sb.append(c);
            } else {
                if (sb.length() > 0) {
                    flagForSpaceInsertion = true;
                }

                if (c == ')') {
                    while (!stack.isEmpty() && stack.peek() != '(') {
                        sb.append(" ").append(stack.pop());
                        if (stack.isEmpty()) {
                            return null;
                        }
                    }

                    stack.pop();
                    continue;
                }

                if (!stack.isEmpty() && stack.peek() != '(' && isLowerPrecedence(c, stack.peek())) {
                    while (!stack.isEmpty() && stack.peek() != '(' && isLowerPrecedence(c, stack.peek())) {
                        sb.append(" ").append(stack.pop());
                    }
                }

                stack.push(c);
            }
        }

        if (stack.contains('(')) {
            return null;
        }

        while (!stack.isEmpty()) {
            sb.append(" ").append(stack.pop());
        }

        return sb.toString();
    }

    private boolean isOperator(char c) {
        return c == '+' || c == '-' || c == '*' ||
                c == '/' || c == '(' || c == ')';
    }

    private boolean isLowerPrecedence(char incoming, char c) {
        switch (incoming) {
            case '*':
            case '/':
                return c != '+' && c != '-';
            case '(':
                return false;
            default:
                return true;
        }
    }
}
